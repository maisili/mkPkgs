{
  description = "mkPkgs - a bridge to pkgs monstrosity, for bootstrap";

  outputs = { self, kor, nixpkgs, lib, overlays }@argz: {

    strok = { };

    datom = {
      nixpkgs ? argz.nixpkgs,
      overlays ? argz.overlays.datom,
      config ? { allowUnfree = true; },
      localSystem,
      crossSystem ? localSystem
    }@mkPkgsArgz:
    let
      inherit (builtins) intersectAttrs;
      inherit (kor.datom) optional;

      clumsyLinuxStdenvStages = {
        lib , localSystem, crossSystem, config, overlays, ...
      }@args:
      import (nixpkgs + /pkgs/stdenv/linux) args;

      clumsyCrossLinuxStdenvStages = {
        lib , localSystem, crossSystem, config, overlays, crossOverlays ? [], ...
      }:
      let
        inherit (lib) init last;

        bootStages = clumsyLinuxStdenvStages {
          inherit lib localSystem overlays;
          crossSystem = localSystem;
          crossOverlays = [];
          config = removeAttrs config [ "replaceStdenv" ];
        };

      in init bootStages ++ [
          # Regular native packages
          (somePrevStage: last bootStages somePrevStage // {
            # It's OK to change the built-time dependencies
            allowCustomOverrides = true;
          })

          # Build tool Packages
          (vanillaPackages: {
            inherit config overlays;
            selfBuild = false;
            stdenv =
              assert vanillaPackages.stdenv.buildPlatform == localSystem;
              assert vanillaPackages.stdenv.hostPlatform == localSystem;
              assert vanillaPackages.stdenv.targetPlatform == localSystem;
              vanillaPackages.stdenv.override { targetPlatform = crossSystem; };
            # It's OK to change the built-time dependencies
            allowCustomOverrides = true;
          })

          # Run Packages
          (buildPackages: {
            inherit config;
            overlays = overlays ++ crossOverlays
            ++ (if crossSystem.isWasm then [(import (nixpkgs + /pkgs/top-level/static.nix))] else []);
            selfBuild = false;
            stdenv = buildPackages.stdenv.override (old: rec {
              buildPlatform = localSystem;
              hostPlatform = crossSystem;
              targetPlatform = crossSystem;

              # Prior overrides are surely not valid as packages built with this run on
              # a different platform, and so are disabled.
              overrides = _: _: {};
              extraBuildInputs = [ ]; # Old ones run on wrong platform
              allowedRequisites = null;

              cc = if crossSystem.useAndroidPrebuilt or false
              then buildPackages."androidndkPkgs_${crossSystem.ndkVer}".clang
              else if targetPlatform.isGhcjs
              then null
              else if crossSystem.useLLVM or false
              then buildPackages.llvmPackages_8.lldClang
              else buildPackages.gcc;

              extraNativeBuildInputs = old.extraNativeBuildInputs
              ++ optional
              (let f = p: !p.isx86 || p.libc == "musl" || p.libc == "wasilibc" || p.isiOS; in f hostPlatform && !(f buildPlatform))
              buildPackages.updateAutotoolsGnuConfigScriptsHook;
            });
          })

        ];

        determineStdenvStages = {
          lib , localSystem, crossSystem, config, overlays, crossOverlays ? []
        }@args:
        let
          inherit clumsyCrossLinuxStdenvStages clumsyLinuxStdenvStages;

          noCross = (crossSystem == localSystem);

          stdenvStagesFn = if noCross then clumsyLinuxStdenvStages
          else clumsyCrossLinuxStdenvStages;

        in stdenvStagesFn args;

        nixpkgsFn = let inherit determineStdenvStages; in
        {
          localSystem , crossSystem ? localSystem ,
          config ? {} , overlays ? [] , crossOverlays ? [],
          stdenvStages ? determineStdenvStages
        }@args:

          let
            configPrime = config;

          in let
            lib = argz.lib.datom;
            inherit (lib) systems evalModules showWarnings;

            config' = if builtins.isFunction configPrime
            then configPrime { inherit pkgs; }
            else configPrime;

            noCross = (args.crossSystem == args.localSystem);

            localSystem = systems.elaborate (
              intersectAttrs { platform = null; } config' // args.localSystem
              );

              crossSystem = if noCross then localSystem else systems.elaborate args.crossSystem;

              configEval = evalModules {
                modules = [
                  (nixpkgs + /pkgs/top-level/config.nix)
                  ({ options, ... }: {
                    _file = "nixpkgs.config";
                    config = intersectAttrs options config';
                  })
                ];
              };

              config = showWarnings configEval.config.warnings
              (config' // removeAttrs configEval.config [ "_module" ]);

              nixpkgsFun = newArgs: nixpkgsFn (args // newArgs);

              allPackages = newArgs:
              import (nixpkgs + /pkgs/top-level/stage.nix) ({ inherit lib nixpkgsFun; } // newArgs);

              boot = import (nixpkgs + /pkgs/stdenv/booter.nix) { inherit lib allPackages; };

              stages = stdenvStages { inherit lib localSystem crossSystem config overlays crossOverlays; };

              pkgs = boot stages;

          in pkgs;

    in nixpkgsFn {
      inherit overlays config localSystem crossSystem;
    };

  };
}
